import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by Bistol on 23/04/2016.
 */
public class AFLHomePage {

    private static WebDriver driver;

    @FindBy(css = ".js-toggle-afl-login-widget")
    private WebElement welcomeUser;

    @FindBy(css = ".js-login-name")
    private WebElement firstName;

    public AFLHomePage(WebDriver driver){
        this.driver = driver;
    }

    public WebElement getWelcomeUser() {
        return welcomeUser;
    }

    public void setWelcomeUser(WebElement welcomeUser) {
        this.welcomeUser = welcomeUser;
    }

    public WebElement getFirstName() {
        return firstName;
    }

    public void setFirstName(WebElement firstName) {
        this.firstName = firstName;
    }

    public boolean assertFirstName(String name){
        if (this.getFirstName().getText()== name)
            return true;
        else
            return false;
    }
}
