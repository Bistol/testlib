import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Bistol on 20/04/2016.
 */
public class AFLLoginPage {

    private static WebDriver driver;


    @FindBy(id = "username")
    private WebElement userName;

    @FindBy(id = "password")
    private WebElement passWord;

    @FindBy(id = "usnername-f-main")
    private WebElement forgotUserName;

    @FindBy(id = "password-f-main")
    private WebElement forgotPassword;

    @FindBy(id = "login-btn")
    private WebElement loginButton;

    @FindBy(id = "cancel-login")
    private WebElement cancelLogin;

    @FindBy(id = "register-link")
    private WebElement registerButton;

    public AFLLoginPage(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement getRegisterButton() {
        return registerButton;
    }

    public void setRegisterButton(WebElement registerButton) {
        this.registerButton = registerButton;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement getUserName() {
        return userName;
    }

    public void setUserName(WebElement userName) {
        this.userName = userName;
    }

    public WebElement getPassWord() {
        return passWord;
    }

    public void setPassWord(WebElement passWord) {
        this.passWord = passWord;
    }

    public WebElement getForgotUserName() {
        return forgotUserName;
    }

    public void setForgotUserName(WebElement forgotUserName) {
        this.forgotUserName = forgotUserName;
    }

    public WebElement getForgotPassword() {
        return forgotPassword;
    }

    public void setForgotPassword(WebElement forgotPassword) {
        this.forgotPassword = forgotPassword;
    }

    public WebElement getLoginButton() {
        return loginButton;
    }

    public void setLoginButton(WebElement loginButton) {
        this.loginButton = loginButton;
    }

    public WebElement getCancelLogin() {
        return cancelLogin;
    }

    public void setCancelLogin(WebElement cancelLogin) {
        this.cancelLogin = cancelLogin;
    }

    public AFLHomePage ValidLogin(String userName,String passWord){
        this.getUserName().sendKeys(userName);
        this.getPassWord().sendKeys(passWord);
        this.getLoginButton().click();
        return PageFactory.initElements(driver,AFLHomePage.class);
    }
}
